module.exports = {
  siteMetadata: {
    title: `Xonatec Blog`,
    name: `Xonatec Blog`,
    siteUrl: `https://blog.xonatec.ch`,
    description: `Xonatec Blog, Technik and More`,
    hero: {
      heading: `Grüezi, auf dem Xonatec Blog`,
      maxWidth: 652,
    },
    social: [
      {
        name: `twitter`,
        url: `https://twitter.com/xonatec`,
      },
      {
        name: `gitlab`,
        url: `https://gitlab.com/xonatec`,
      },
    ],
  },
  plugins: [
    {
      resolve: "@narative/gatsby-theme-novela",
      options: {
        contentPosts: "content/posts",
        contentAuthors: "content/authors",
        basePath: "/",
        authorsPage: false,
        sources: {
          local: true,
          // contentful: true,
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Novela by Narative`,
        short_name: `Novela`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#fff`,
        display: `standalone`,
        icon: `src/assets/favicon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-netlify-cms`,
      options: {
      },
    },
  ],
};
